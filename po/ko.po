# Korean translation of epour.master.
# Copyright (C) 2015 epour translators.
# This file is distributed under the same license as the epour package.
# Seong-ho Cho <shcho@gnome.org>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: epour.master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-22 09:05+0300\n"
"PO-Revision-Date: 2015-10-29 21:05+0900\n"
"Last-Translator: Seong-ho Cho <shcho@gnome.org>\n"
"Language-Team: Enlightenment Korea\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.5\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Language: ko\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: ../epour/gui/Widgets.py:106
msgid "Close"
msgstr "닫기"

#: ../epour/gui/Widgets.py:119
msgid "OK"
msgstr "확인"

#: ../epour/gui/Widgets.py:128
msgid "Confirm exit"
msgstr "나가기 확인"

#: ../epour/gui/Widgets.py:129
msgid "Are you sure you wish to exit Epour?"
msgstr "정말로 Epour를 종료하시겠습니까?"

#: ../epour/gui/Widgets.py:131
msgid "Yes"
msgstr "예"

#: ../epour/gui/Widgets.py:135
msgid "No"
msgstr "아니요"

#: ../epour/gui/TorrentProps.py:87
msgid "Enable/disable file download"
msgstr "파일 다운로드 활성/비활성"

#: ../epour/gui/TorrentProps.py:121
msgid "Invalid torrent handle."
msgstr "잘못된 토렌트 핸들입니다."

#: ../epour/gui/TorrentProps.py:146
msgid "Torrent info"
msgstr "토렌트 정보"

#: ../epour/gui/TorrentProps.py:154
msgid "Torrent settings"
msgstr "토렌트 설정"

#: ../epour/gui/TorrentProps.py:162
msgid "Torrent status"
msgstr "토렌트 상태"

#: ../epour/gui/TorrentProps.py:171
msgid "Magnet URI"
msgstr "마그넷 URI"

#: ../epour/gui/TorrentProps.py:179
msgid "Copy"
msgstr "복사"

#: ../epour/gui/TorrentProps.py:206
#, python-format
msgid "Epour - Files for torrent: %s"
msgstr "epour - 토렌트 전용 파일: %s"

#: ../epour/gui/TorrentProps.py:248
msgid "Select all"
msgstr "전체 선택"

#: ../epour/gui/TorrentProps.py:252
msgid "Select none"
msgstr "선택 취소"

#: ../epour/gui/TorrentProps.py:391
msgid "Private"
msgstr "비공개"

#: ../epour/gui/TorrentProps.py:442
msgid "Storage path"
msgstr "저장소 경로"

#: ../epour/gui/TorrentProps.py:448
msgid "Select"
msgstr "선택"

#: ../epour/gui/TorrentProps.py:500
msgid "disabled"
msgstr "비활성"

#: ../epour/gui/TorrentProps.py:536 ../epour/gui/__init__.py:428
msgid "Queued"
msgstr "대기열에 있음"

#: ../epour/gui/TorrentProps.py:536 ../epour/gui/__init__.py:428
msgid "Checking"
msgstr "검사 중"

#: ../epour/gui/TorrentProps.py:536 ../epour/gui/__init__.py:428
msgid "Downloading metadata"
msgstr "메타데이터 다운로드 중"

#: ../epour/gui/TorrentProps.py:536 ../epour/gui/__init__.py:429
msgid "Downloading"
msgstr "다운로드 중"

#: ../epour/gui/TorrentProps.py:537 ../epour/gui/__init__.py:429
msgid "Finished"
msgstr "완료"

#: ../epour/gui/TorrentProps.py:537 ../epour/gui/__init__.py:429
msgid "Seeding"
msgstr "배포 중"

#: ../epour/gui/TorrentProps.py:537 ../epour/gui/__init__.py:429
msgid "Allocating"
msgstr "할당 중"

#: ../epour/gui/TorrentProps.py:537 ../epour/gui/__init__.py:430
msgid "Checking resume data"
msgstr "복귀 데이터 검사 중"

#: ../epour/gui/__init__.py:90
msgid "Add torrent"
msgstr "토렌트 추가"

#: ../epour/gui/__init__.py:103
msgid "Pause Session"
msgstr "세션 일시 정지"

#: ../epour/gui/__init__.py:107
msgid "Resume Session"
msgstr "세션 재개"

#: ../epour/gui/__init__.py:123
msgid "Preferences"
msgstr "기본 설정"

#: ../epour/gui/__init__.py:126
msgid "General"
msgstr "일반"

#: ../epour/gui/__init__.py:130
msgid "Proxy"
msgstr "프록시"

#: ../epour/gui/__init__.py:134 ../epour/gui/__init__.py:335
msgid "Session"
msgstr "세션"

#: ../epour/gui/__init__.py:138
msgid "Exit"
msgstr "나가기"

#: ../epour/gui/__init__.py:222
msgid "Torrent {} has finished downloading."
msgstr "{} 토렌트에서 다운로드를 끝냈습니다."

#: ../epour/gui/__init__.py:366
msgid "Peer connections"
msgstr "피어 연결"

#: ../epour/gui/__init__.py:374
msgid "Upload slots"
msgstr "업로드 슬롯"

#: ../epour/gui/__init__.py:383
msgid "Listening"
msgstr "감청 중..."

#: ../epour/gui/__init__.py:450
msgid "Invalid torrent"
msgstr "잘못된 토렌트"

#: ../epour/gui/__init__.py:456
msgid "{:.0%} complete, ETA: {} (Down: {}/s Up: {}/s Queue pos: {})"
msgstr "{:.0%} 완료, ETA: {} (다운: {}/s 업: {}/s 대기열 위치: {})"

#: ../epour/gui/__init__.py:537
msgid "Resume"
msgstr "재개"

#: ../epour/gui/__init__.py:537
msgid "Pause"
msgstr "일시 정지"

#: ../epour/gui/__init__.py:542
msgid "Queue"
msgstr "큐"

#: ../epour/gui/__init__.py:544
msgid "Up"
msgstr "위"

#: ../epour/gui/__init__.py:547
msgid "Down"
msgstr "아래"

#: ../epour/gui/__init__.py:550
msgid "Top"
msgstr "상단"

#: ../epour/gui/__init__.py:553
msgid "Bottom"
msgstr "하단"

#: ../epour/gui/__init__.py:556
msgid "Remove torrent"
msgstr "토렌트 제거"

#: ../epour/gui/__init__.py:560
msgid "and data files"
msgstr "데이터 파일 포함"

#: ../epour/gui/__init__.py:565
msgid "Force reannounce"
msgstr "강제 재알림"

#: ../epour/gui/__init__.py:578
msgid "Force DHT reannounce"
msgstr "강제 DHT 재알림"

#: ../epour/gui/__init__.py:582
msgid "Scrape tracker"
msgstr "트래커 긁어모으기"

#: ../epour/gui/__init__.py:592
msgid "Force re-check"
msgstr "강제 재검사"

#: ../epour/gui/__init__.py:608
msgid "Torrent properties"
msgstr "토렌트 속성"

#: ../epour/gui/__init__.py:648
msgid "Time when added"
msgstr "추가 시각"

#: ../epour/gui/__init__.py:649
msgid "Time when completed"
msgstr "완료 시각"

#: ../epour/gui/__init__.py:650
msgid "All time downloaded"
msgstr "전체 다운로드 시간"

#: ../epour/gui/__init__.py:651
msgid "All time uploaded"
msgstr "전체 업로드 시간"

#: ../epour/gui/__init__.py:652
msgid "Total wanted done"
msgstr "전체 요구 완료"

#: ../epour/gui/__init__.py:653
msgid "Total wanted"
msgstr "전체 요구"

#: ../epour/gui/__init__.py:654
msgid "Total downloaded this session"
msgstr "이 세션에서의 전체 다운로드 용량"

#: ../epour/gui/__init__.py:656
msgid "Total uploaded this session"
msgstr "이 세션에서의 전체 업로드 용량"

#: ../epour/gui/__init__.py:657
msgid "Total failed"
msgstr "전체 실패 횟수"

#: ../epour/gui/__init__.py:658
msgid "Number of seeds"
msgstr "시드 갯수"

#: ../epour/gui/__init__.py:659
msgid "Number of peers"
msgstr "피어 수"

#: ../epour/gui/__init__.py:678 ../epour/gui/__init__.py:721
msgid "N/A"
msgstr "없음"

#: ../epour/gui/__init__.py:702
#, python-format
msgid "Pieces (scaled 1:%d)"
msgstr "조각(1:%d 비율)"

#: ../epour/gui/Preferences.py:97
msgid "Epour General Preferences"
msgstr "epour 일반 기본 설정"

#: ../epour/gui/Preferences.py:111
msgid "Move completed torrents"
msgstr "작업을 완료한 토렌트 이동"

#: ../epour/gui/Preferences.py:119
msgid "Completed torrents"
msgstr "작업을 완료한 토렌트"

#: ../epour/gui/Preferences.py:148
msgid "Delete original .torrent file when added"
msgstr "원본 .torrent 파일을 목록에 추가하고 나면 삭제"

#: ../epour/gui/Preferences.py:156
msgid "Ask for confirmation on exit"
msgstr "종료 시 확인"

#: ../epour/gui/Preferences.py:165
msgid "Torrents to be added from dbus or command line open a dialog"
msgstr "D-Bus 또는 명령줄에서 연 토렌트로 대화상자 열기"

#: ../epour/gui/Preferences.py:200
msgid "Change path"
msgstr "경로 바꾸기"

#: ../epour/gui/Preferences.py:237
msgid "Listen port (range)"
msgstr "감청 포트(범위)"

#: ../epour/gui/Preferences.py:276
msgid "Epour Proxy Preferences"
msgstr "epour 프록시 기본 설정"

#: ../epour/gui/Preferences.py:279
msgid "Proxy for torrent peer connections"
msgstr "토렌트 피어 연결 프록시"

#: ../epour/gui/Preferences.py:281
msgid "Proxy for torrent web seed connections"
msgstr "토렌트 웹 시드 연결 프록시"

#: ../epour/gui/Preferences.py:283
msgid "Proxy for tracker connections"
msgstr "트래커 연결 프록시"

#: ../epour/gui/Preferences.py:285
msgid "Proxy for DHT connections"
msgstr "DHT 연결 프록시"

#: ../epour/gui/Preferences.py:320
msgid "Proxy type"
msgstr "프록시 형식"

#: ../epour/gui/Preferences.py:330
msgid "Hostname"
msgstr "호스트 이름"

#: ../epour/gui/Preferences.py:341
msgid "Port"
msgstr "포트"

#: ../epour/gui/Preferences.py:349
msgid "Username"
msgstr "사용자 이름"

#: ../epour/gui/Preferences.py:360
msgid "Password"
msgstr "암호"

#: ../epour/gui/Preferences.py:372
msgid "Proxy hostname lookups"
msgstr "프록시 호스트 이름 검색"

#: ../epour/gui/Preferences.py:381
msgid "Proxy peer connections"
msgstr "프록시 피어 연결 수"

#: ../epour/gui/Preferences.py:391 ../epour/gui/Preferences.py:459
msgid "Apply"
msgstr "적용"

#: ../epour/gui/Preferences.py:410
#, python-format
msgid "%s settings saved"
msgstr "%s 설정을 저장했습니다"

#: ../epour/gui/Preferences.py:422
msgid "Encryption settings"
msgstr "암호화 설정"

#: ../epour/gui/Preferences.py:434
msgid "Incoming encryption"
msgstr "들어오는 데이터 암호화"

#: ../epour/gui/Preferences.py:440
msgid "Outgoing encryption"
msgstr "나가는 데이터 암호화"

#: ../epour/gui/Preferences.py:446
msgid "Allowed encryption level"
msgstr "허용 암호화 수준"

#: ../epour/gui/Preferences.py:453
msgid "Prefer RC4 ecryption"
msgstr "적당한 RC4 암호화"

#: ../epour/gui/Preferences.py:483
msgid "Epour Session Preferences"
msgstr "epour 세션 기본 설정"

#: ../epour/gui/TorrentSelector.py:61
msgid "Seed Mode"
msgstr "시드 모드"

#: ../epour/gui/TorrentSelector.py:64
msgid "Share Mode"
msgstr "공유 모드"

#: ../epour/gui/TorrentSelector.py:65
msgid "Apply IP Filter"
msgstr "IP 필터 적용"

#: ../epour/gui/TorrentSelector.py:68
msgid "Duplicate Is Error"
msgstr "복제물에 오류가 있습니다"

#: ../epour/gui/TorrentSelector.py:71
msgid "Super Seeding"
msgstr "수퍼 시드"

#: ../epour/gui/TorrentSelector.py:72
msgid "Sequential Download"
msgstr "순차 다운로드"

#: ../epour/gui/TorrentSelector.py:73
msgid "Use Resume Save Path"
msgstr "재개 저장 경로 사용"

#: ../epour/gui/TorrentSelector.py:79
msgid ""
"If Seed Mode is set, Epour will assume that all files are\n"
"present for this torrent and that they all match the hashes in the\n"
"torrent file. Each time a peer requests to download a block, the\n"
"piece is verified against the hash, unless it has been verified\n"
"already. If a hash fails, the torrent will automatically leave the\n"
"seed mode and recheck all the files. The use case for this mode is\n"
"if a torrent is created and seeded, or if the user already know\n"
"that the files are complete, this is a way to avoid the initial\n"
"file checks, and significantly reduce the startup time.\n"
"\n"
"Setting Seed Mode on a torrent without metadata (a .torrent\n"
"file) is a no-op and will be ignored."
msgstr ""
"시드 모드를 설정하면, Epour는 이 토렌트에 파일이 모두 존재한다고\n"
"가정할 뿐만 아니라, 토렌트 파일에 일치하는 모든 해시가 있다고 가정합니다.\n"
"종종 피어는 조각 단위로 다운로드 요청을 하며,검증되지 않은 각각의\n"
"조각은 해시로 검증합니다.해시에 문제가 있다면, 토렌트는 자동으로\n"
"시드 모드에서 빠져나간 후, 모든 파일을 다시 점검합니다. 이 모드를\n"
"사용하는 경우는 토렌트를 만들고 시드로 뿌리거나 사용자가 완전하게 파일을\n"
"갖추어 놓은 사실을 이미 알고 있을 경우이며, 초기 파일 검사를\n"
"무시하여 시작 시간을 확실히 줄입니다.\n"
"메타데이터(.torrent 파일)가 배제된 토렌트의 시드 모드\n"
"설정은 no-op로서 무시됩니다."

#: ../epour/gui/TorrentSelector.py:124
#, python-format
msgid ""
"Determines if the torrent should be added in share mode or not.\n"
"Share mode indicates that we are not interested in downloading the\n"
"torrent, but merely want to improve our share ratio (i.e. increase\n"
"it). A torrent started in share mode will do its best to never\n"
"download more than it uploads to the swarm. If the swarm does not\n"
"have enough demand for upload capacity, the torrent will not\n"
"download anything. This mode is intended to be safe to add any\n"
"number of torrents to, without manual screening, without the risk\n"
"of downloading more than is uploaded.\n"
"\n"
"A torrent in share mode sets the priority to all pieces to 0,\n"
"except for the pieces that are downloaded, when pieces are decided\n"
"to be downloaded. This affects the progress bar, which might be set\n"
"to \"100% finished\" most of the time. Do not change file or piece\n"
"priorities for torrents in share mode, it will make it not work.\n"
"\n"
"The share mode has one setting, the share ratio target."
msgstr ""
"토렌트를 공유모드로 추가할 지 아닐지를 결정합니다.\n"
"공유 모드는 토렌트 다운로드에 관심없지만 간혹 공유 상태를 \n"
"개선 (예: 공유율 증가) 하려는 경우를 의미합니다. 공유모드로 시작한 \n"
"토렌트는 다운로드하는 용량보다 다수에게 업로드하는 용량을 늘리는\n"
"방향으로 동작합니다. 다수가 업로드 용량을 충분히 요청하지 않으면, \n"
"토렌트를 다운로드하지 않습니다. 이 모드는 여러 토렌트를 직접 검토하거나 \n"
"업로드 한 부분보다 더 많이 다운로드하려는 시도로 발생할 수 있는 위험을 \n"
"피하며 안전하게 여러 토렌트를 추가합니다.\n"
"\n"
"공유 모드에서의 토렌트는 모든 조각의 우선 순위를 0으로 설정하며,\n"
"조각을 다운로드하기로 설정한 시점의 다운로드 조각은 제외합니다.\n"
"이는 대부분의 경우 진행 표시줄에 \"100% finished\"로 설정하는 진행\n"
"표시줄에 영향을 줍니다. 파일을 바꾸거나 공유 모드의 토렌트 조각의\n"
"우선순위를 바꾸지 마십시오. 동작이 멈출 수도 있습니다.\n"
"\n"
"공유 모드는 공유 비율을 대상으로 한 단일 설정을 보유하고 있습니다."

#: ../epour/gui/TorrentSelector.py:145
msgid ""
"Determines if the IP filter should apply to this torrent or not. By\n"
"default all torrents are subject to filtering by the IP filter\n"
"(i.e. this flag is set by default). This is useful if certain\n"
"torrents needs to be excempt for some reason, being an auto-update\n"
"torrent for instance."
msgstr ""
"IP 필터를 토렌트에 적용할지 여부를 설정합니다. 기본적으로 모든 토렌트에는\n"
"IP 필터로 IP를 걸러내는 부분이 있습니다(이 플래그 설정은 기본).\n"
"예를 들어 자동 업데이트를 수행하는 토렌트와 같이 어떤 이유로 인해\n"
"토렌트에서 일부 IP를 제외해야 할 경우 유용합니다."

#: ../epour/gui/TorrentSelector.py:196
msgid ""
"Sets the torrent into super seeding mode. If the torrent is not a\n"
"seed, this flag has no effect."
msgstr ""
"수퍼 시딩 모드로 토렌트를 설정합니다. 토렌트가 시드가 아니라면,\n"
"플래그는 아무런 영향을 주지 않습니다."

#: ../epour/gui/TorrentSelector.py:202
msgid "Sets the sequential download state for the torrent."
msgstr "토렌트에 순차 다운로드를 설정합니다."

#: ../epour/gui/TorrentSelector.py:207
msgid ""
"If this flag is set, the save path from the resume data file,\n"
"if present, is honored. This defaults to not being set, in\n"
"which case the save_path specified in add_torrent_params is\n"
"always used."
msgstr ""
"이 플래그를 설정하면, 재개 데이터 파일의 저장 경로가 있을 경우\n"
"우선합니다. 이 기본값은 add_torrent_params에 설정한 \n"
"save_path를 항상 사용하는 경우 설정하지 않습니다."

#: ../epour/gui/TorrentSelector.py:217
msgid "Add Torrent"
msgstr "토렌트 추가"

#: ../epour/gui/TorrentSelector.py:243
msgid "Enter torrent file path / magnet URI / info hash"
msgstr "토렌트파일 경로 / 마그넷 URI / 정보 해시를 입력하십시오"

#: ../epour/gui/TorrentSelector.py:252
msgid "Select file"
msgstr "파일 선택"

#: ../epour/gui/TorrentSelector.py:259
msgid "Advanced Options"
msgstr "고급 옵션"

#: ../epour/gui/TorrentSelector.py:282
msgid "Name"
msgstr "이름"

#: ../epour/gui/TorrentSelector.py:283
msgid "Tracker ID"
msgstr "트래커 ID"

#. Downloaded data is saved in this path
#: ../epour/gui/TorrentSelector.py:295
msgid "Data Save Path"
msgstr "데이터 저장 경로"

#: ../epour/gui/TorrentSelector.py:344
msgid "Max Uploads"
msgstr "최대 업로드 수"

#: ../epour/gui/TorrentSelector.py:345
msgid "Max Connections"
msgstr "최대 연결 수"

#: ../epour/gui/TorrentSelector.py:356
msgid "Upload Limit"
msgstr "업로드 제한"

#: ../epour/gui/TorrentSelector.py:357
msgid "Download Limit"
msgstr "다운로드 제한"

#: ../epour/gui/TorrentSelector.py:383
msgid "Ok"
msgstr "확인"

#: ../epour/gui/TorrentSelector.py:387
msgid "Cancel"
msgstr "취소"
